# chart-updater

## Helm chart auto-packager & repo updater

@author Ruben

> Until I find a better solution ;)

## Usage

The script expects `HELM_REPO_USER`, `HELM_REPO_PASS` and `HELM_REPO_URL` env variables.

The URL should point to a webdav http(s) server

## How it works

* Package helm chart in repo's ./chart directory
* Download index from chart repo
* Merge index
* Upload package & merged index to webdav repo server

### Disclaimer

Yes!